import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  CheckBox,
} from 'react-native';

const App = () => {
  const [enteredText, setEnteredText] = useState('');
  const [outputList, setOutputList] = useState([]);
  const inputHandler = text => {
    setEnteredText(text);
  };

  const clickHandler = () => {
    setOutputList(currentList => [
      ...currentList,
      {content: enteredText, checked: false},
    ]);
    setEnteredText('');
  };

  const tapHandler = elementID => {
    setOutputList(currentList => {
      return currentList.filter((listItem, index) => index !== elementID);
    });
  };

  const selectionHandler = elementID => {
    setOutputList(currentList => {
      const updatedList = currentList.map((li, index) => {
        if (index === elementID) {
          return {
            ...li,
            checked: !li.checked,
          };
        }
        return li;
      });
      return updatedList;
    });
  };
  return (
    <View style={styles.screen}>
      <View style={styles.titleContainer}>
        <Text> TODO APP </Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.textBox}
          placeholder="Type here"
          value={enteredText}
          onChangeText={inputHandler}
        />
        <Button title="Add Item" onPress={clickHandler} />
      </View>
      <ScrollView>
        {outputList.map((li, index) => (
          <View key={index} style={styles.listContainer}>
            <TouchableOpacity
              key={index}
              activeOpacity={0.8}
              onPress={() => console.log('Short Press')}
              onLongPress={() => tapHandler(index)}>
              <View style={styles.listItem}>
                <Text>
                  {index + 1}. {li.content}
                </Text>
              </View>
            </TouchableOpacity>
            <CheckBox
              value={li.checked}
              onValueChange={() => selectionHandler(index)}
            />
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    padding: 20,
  },
  listItem: {
    width: 240,
    padding: 10,
    backgroundColor: '#ccc',
    borderColor: 'black',
    borderWidth: 1,
    marginVertical: 10,
    marginRight: 10,
  },
  titleContainer: {
    alignItems: 'center',
    padding: 20,
  },
  button: {
    width: 100,
  },
  textBox: {
    width: 240,
    height: 40,
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
export default App;
